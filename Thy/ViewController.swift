//
//  ViewController.swift
//  Thy
//
//  Created by erkanugurlu on 17/05/2017.
//  Copyright © 2017 erkanugurlu. All rights reserved.
//

import UIKit
import SwiftyJSON
import PKHUD

var ports : [JSON]?

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let link = "https://api.turkishairlines.com/test/getPortList?airlineCode=TK&languageCode=TR"
        
        self.callService(link, method: "GET", body: nil, completion: { (json:JSON?) in
            ports = json?["data"]["Port"].array
            self.tableView.dataSource = self
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error:Service.HSPError) in
            log.error(error)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ports?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.textLabel?.text = ports?[indexPath.row]["City"]["Code"].stringValue
        cell.detailTextLabel?.text = ports?[indexPath.row]["Country"]["LanguageInfo"]["Language"]["Name"].stringValue
        
        return cell
    }
    
}


let appName = "Thy"

enum ExceptionEnum : Int {
    case
    InvalidUser = 1000,
    UserExist = 1001,
    InvalidRole = 1002,
    InvalidActivation = 1003,
    InvalidPin = 1004,
    InvalidLoginCount = 1005,
    InvalidSignature = 1006,
    InvalidPassword = 1007,
    InvalidMobileNoFormat = 1008,
    InvalidPasswordFormat = 1009,
    SendSmsException = 1010,
    OtpExpired = 1011,
    InvalidOtp = 1012,
    InvalidJWTToken = 1013,
    DailySmsLimitExceeded = 1014,
    InvalidEmail = 1015,
    NotVerifiedEmail = 1016,
    CommonException = 9999
}

struct Service {
    static var jwtToken:String?
    static let baseUrl = "https://hesapodertest.azurewebsites.net"
    
    enum Api : String {
        case register = "/v1/auth/register"
        case validateOtp = "/v1/auth/validateOtp"
        case createPassword = "/v1/auth/createPassword"
        case activation = "/v1/auth/activation"
        case login = "/v1/auth/login"
        case reSendOtp = "/v1/auth/reSendOtp"
        case reRegister = "/v1/auth/reRegister"
        case getPubKey = "/v1/auth/getUserPublic"
        case lostPassword = "/v1/auth/lostPassword"
    }
    
    struct HSPError: Error {
        var code:Int
        var message:String?
        
        init(code:Int,message:String?) {
            self.code = code
            self.message = message
        }
    }
}


extension UIViewController {
    
//curl --include --header "apisecret:4e698eed490d4199a488a20ad5ac0929" --header "apikey:l7xx14240f1ea31b4b448ce05797dee1444d" --request GET 'https://api.turkishairlines.com/test/getPortList?airlineCode=TK&languageCode=TR'
    
    func callService(_ urlPath:String,method:String = "POST",body:AnyObject? = nil, completion:@escaping ((_ json:JSON?)->()),fail:@escaping ((_ error:Service.HSPError)->())){
        
        var request = URLRequest(url: URL(string: urlPath)!)
        request.httpMethod = method
        request.setValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("4e698eed490d4199a488a20ad5ac0929",forHTTPHeaderField: "apisecret")
        request.setValue("l7xx14240f1ea31b4b448ce05797dee1444d",forHTTPHeaderField: "apikey")
//        if let jwtToken = Service.jwtToken {
//            request.setValue("Bearer "+jwtToken, forHTTPHeaderField: "Authorization")
//        }
        
        self.showLoading()
        
        do {
            if let body = body {
                let data = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue:0))
                request.httpBody = data
                
//                log.debug("\(urlPath) : request => \(body)")
            }
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                self.hideLoading()
                
                if let data = data {
                    let json = try! JSON(data:data)
                    if (json.null != nil) {
                        log.error("JSON parse error")
                        self.showPopup(content: "Problem oluştu lütfen daha sonra tekrar deneyin")
                    }
                    
                    
                    if let isSuccess = json["status"].string, isSuccess == "SUCCESS" {
//                        log.debug("\(urlPath) : response => \(json) \n")
//                        if let jw = json["jwtToken"].string {
//                            Service.jwtToken = jw
//                        }
                        
                        completion(json)
                    } else {
                        log.error("Error: \(json)")
                        fail(Service.HSPError(code: json["responseCode"].intValue, message: json["responseMessage"].string))
                    }
                } else {
                    self.showPopup(content: error?.localizedDescription)
                }
                
            }) .resume()
        } catch {
            self.hideLoading()
            self.showPopup(content: "Problem oluştu lütfen daha sonra tekrar deneyin")
            log.error("error")
        }
        
        
    }
    
}

extension UIViewController {
    
    func showPopup(content:String?){
        showPopup(headline:appName ,content: content, btn1Title: "Tamam", completion1: nil, btn2Title: nil, completion2: nil)
    }
    
    func showPopup(content:String?, completion:(()->())? = nil){
        showPopup(headline: appName ,content: content, btn1Title: "Tamam", completion1: completion, btn2Title: nil, completion2: nil)
    }
    
    func showPopup(headline:String = appName, content:String?,btnTitle:String?, completion:(()->())? = nil){
        showPopup(headline: headline ,content: content, btn1Title: btnTitle, completion1: completion, btn2Title: nil, completion2: nil)
    }
    
    func showPopup(headline:String = appName, content:String?, btn1Title:String?,completion1:(()->())? = nil, btn2Title:String?, completion2:(()->())? = nil){
        
        let presentedVC = UIAlertController(title: headline, message: content, preferredStyle: UIAlertControllerStyle.alert)
        
        let action1 = UIAlertAction(title: btn1Title, style: UIAlertActionStyle.default) { (alert) in
            if let completion1 = completion1 {
                completion1()
            }
        }
        presentedVC.addAction(action1)
        
        if btn2Title != nil {
            let action2 = UIAlertAction(title: btn2Title, style: UIAlertActionStyle.default) { (alert) in
                if let completion2 = completion2 {
                    completion2()
                }
            }
            presentedVC.addAction(action2)
        }
        
        DispatchQueue.main.async { [unowned self]() -> Void in
            if let parent =  self.parent {
                //if already has presentedVC, dissmiss it before.
                if let navigationC = self.parent?.presentedViewController {
                    navigationC.dismiss(animated: false, completion: { () -> Void in
                        DispatchQueue.main.async {
                            parent.present(presentedVC, animated: true, completion: nil)
                        }
                    })
                } else {
                    parent.present(presentedVC, animated: true, completion: nil)
                }
            } else {
                if  let navigationC = self.presentedViewController {
                    navigationC.dismiss(animated: false, completion: { () -> Void in
                        DispatchQueue.main.async {
                            self.present(presentedVC, animated: true, completion: nil)
                        }
                    })
                } else {
                    self.present(presentedVC, animated: true, completion: nil)
                }
                
            }
        }
    }
    
}

extension UIViewController {
    
    func showLoading() {
        DispatchQueue.main.async {
            HUD.show(.progress)
        }
    }
    
    func hideLoading(){
        DispatchQueue.main.async {
            HUD.hide()
        }
    }
    
}

extension UIViewController {
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
}
