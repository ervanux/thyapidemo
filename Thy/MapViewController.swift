//
//  MapViewController.swift
//  Thy
//
//  Created by erkanugurlu on 17/05/2017.
//  Copyright © 2017 erkanugurlu. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import SafariServices


class CustomPin : MKPointAnnotation {
    var portIndex : Int!
}

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapview: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.mapview.delegate = self
        
        if let ports = ports {
            for (index,port) in ports.enumerated() {
                
//                guard port["IsSPAArrival"].boolValue else {
//                    continue
//                }
                
                let longitude = port["Coordinate"]["longitude"].doubleValue
                let latitude = port["Coordinate"]["latitude"].doubleValue
                let newYorkLocation = CLLocationCoordinate2DMake(latitude, longitude)
                // Drop a pin
                let dropPin = CustomPin()
                dropPin.portIndex = index
                dropPin.coordinate = newYorkLocation
                dropPin.title = port["Code"].stringValue + " - " + port["City"]["LanguageInfo"]["Language"][0]["Name"].stringValue
                self.mapview.addAnnotation(dropPin)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


extension MapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            let btn = UIButton(type: .detailDisclosure)
            btn.addTarget(self, action: #selector(MapViewController.callToNumber(sender:)), for: .touchUpInside)
            annotationView?.rightCalloutAccessoryView = btn
        }
        
        if let annotationView = annotationView {
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "locationIcon")
        }
        
        return annotationView
    }
    
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//    }
    
    func callToNumber(sender: UIButton!){
        let svc = SFSafariViewController(url: URL(string:"https://www.wolframalpha.com/input/?i=konya")!)
        self.present(svc, animated: true, completion: nil)

    }
}
